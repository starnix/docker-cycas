FROM debian:latest
COPY cycas39-en_3.90-5_i386.deb /root
RUN dpkg --add-architecture i386 && apt-get -y update && apt-get -y install /root/cycas39-en_3.90-5_i386.deb && apt-get -y install x11-xserver-utils povray povray-includes imagemagick
RUN rm -f /root/cycas39-en_3.90-5_i386.deb
RUN useradd -s /bin/bash -m cycas && mkdir /home/cycas/CYCAS3 && chown cycas:cycas /home/cycas/CYCAS3 && chmod 777 /home/cycas/CYCAS3
COPY x-povray /usr/local/bin/x-povray
RUN chmod 777 /usr/local/bin/x-povray
USER cycas
WORKDIR /home/cycas

